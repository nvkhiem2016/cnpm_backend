import database from './database';
import server from './server';

import secret from './secret';
const config = {

    server,
    database,
    secret
}

export  {
    config as default,
    server,
    database,
    secret
};