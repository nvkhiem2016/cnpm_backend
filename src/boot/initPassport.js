import passport from "passport";
import JwtPassport from "passport-jwt";
import { ExtractJwt } from "passport-jwt";
import { secret } from "../../config/index";
import User from "../models/User";
passport.use(
  new JwtPassport.Strategy({
    jwtFromRequest: ExtractJwt.fromHeader("access-token"),
    secretOrKey: secret.SECRET_KEY
  },
  async (payload, done) => {
    try {
      //Find user in DB
      const user = await User.findById(payload.sub);
			if (!user) return done(null, false);
			done(null, user);
    } catch (error) {
      done(error, false);
    }
  }
));
