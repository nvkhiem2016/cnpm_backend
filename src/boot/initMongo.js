import db from './../../config/database';
import mongoose from 'mongoose';

const initMongo = ()=>{
    mongoose.connect(db.url,{
        useNewUrlParser: true
    })
    .then(()=>{
        console.log('[MongoDB] Connected mongodb server');
    })
    .catch((err)=>{
        console.log('[MongoDB] Failed when try connect Mongodb.');
        console.log('[Error Mongodb] ' + err);
    })
}
export default initMongo;