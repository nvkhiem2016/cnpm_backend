import schedule from 'node-schedule';
import { io } from './initServer';

function initSocket(socket) {
  console.log("[Socket] Client connect : ", socket.id)
  //Scan user khi 1 user đăng nhập, sẽ scan các users đang online và emit hiển thị cho users online

  socket.on("SCAN_USER", (data) => {
    socket.broadcast.emit("SCAN_USER", {
      socketid: socket.id
    })
  })
  socket.on("SCAN_USER_DONE", (data) => {
    console.log(data," SCAN_USER_DONE ")
    socket.to(data.socketid).emit("SCAN_USER_DONE",{
      ...data,
      socketid: socket.id
    })
  })
  socket.on("EMIT_USER", (data) => {
    socket.broadcast.emit("EMIT_USER", {
      ...data,
      socketid: socket.id
    })
  })
  //-------------------------------------------------------------
  //user disconnect 
  socket.on("disconnect",(data)=>{
    socket.broadcast.emit("REMOVE_USER", {
      socketid: socket.id
    })
  });
}
export default initSocket;