import initServer,{io} from './initServer';
import initSocket from './initSocket';
import config from './../../config/server';
const initExpress = async ()=>{
    let {PORT,URLAPI,IP}=config;
    io.on("connection",initSocket);
    initServer.listen(PORT,(error)=>{
        if(error) {
            console.log(error)
        }
        console.log(`[API RUNNING]:http://${IP}:${PORT}/${URLAPI}`);
    })
}

export default initExpress;