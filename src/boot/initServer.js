import express from 'express';
import socketio from 'socket.io';
import cors from 'cors'
import http from 'http';
import cookieParser from 'cookie-parser';
import logger from 'morgan'
import path from 'path'
import ValidationHelp from "../helpers/validation";

const app = express();
//import middleware
import response from './../middlewares/response'
//import Router
import indexRouter from "../routes";
//Expess validator
const Validation = new ValidationHelp();

app.use(express.static('public'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(cookieParser());
app.use(logger("dev"));
app.use(response);
app.options('*', cors());
//Config express-validator
app.use(Validation.provideDefaultValidator());

//Config passport
require("./initPassport");
//Config socket

//Config router
app.use("/", indexRouter);
// app.use(express.static(path.join(__dirname, 'build')));

// app.get('/', function(req, res) {
//   res.sendFile(path.join(__dirname, 'build', 'index.html'));
// });

const initServer = http.createServer(app);
const io = socketio(initServer);

export  {
    initServer as default,
    io,
};