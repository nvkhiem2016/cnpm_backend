import mongoose, { Schema } from 'mongoose'
 

const CommentSchema = new Schema({
  createBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  idTask: {
    type: Schema.Types.ObjectId,
    ref: 'Task'
  },
  content: {
    type: String,
    required: true
  },
  listLike: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  createAt: {
    type: Date,
    default: Date.now
  },
 

})
const CommentModel = mongoose.model('Comment', CommentSchema);
export default CommentModel;