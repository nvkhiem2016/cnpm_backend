import mongoose,{Schema} from 'mongoose'

const UserSchema = new Schema({
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,//String
        required:true
    },
    name:{
        type:String,
        default:''
    },
    gender:{
        type:String,
        default:''
    },
    address:{
        type:String,
        default:''        
    },
    birthday:{
        type:Date,
        default:Date.now()
    },
})
const UserModel = mongoose.model('User',UserSchema);
export default UserModel;