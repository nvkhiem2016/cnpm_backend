import mongoose, { Schema } from 'mongoose'


const MessageSchema = new Schema({
  idUser: {
    type: Schema.Types.ObjectId,
    ref: "User"
  },
  content:{
    type:String
  },
  idConversation:{
    type: Schema.Types.ObjectId,
    ref: "Conversation"
  },
  createAt:{
    type:Date,
    default:Date.now
  }
})
const MessageModel = mongoose.model('Message', MessageSchema);
export default MessageModel;