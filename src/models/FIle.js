import mongoose, { Schema } from 'mongoose'
//import handler
import TaskHandler from "../handlers/task";
//new Object
const taskHandler = new TaskHandler();

const FileSchema = new Schema({
  name: {
    type: String,
    default:"General"
  },
  fileinfor: [{
    name:String,
    path:String
  }],
  idTask: {
    type: Schema.Types.ObjectId,
    ref: 'Task',
    default:null
  },
  idProject: {
    type: Schema.Types.ObjectId,
    ref: 'Project',
    default: null
  },
  createBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
},{timestamps:true})
FileSchema.pre("save",async function (next) {
  var file = this;
  if(file.idTask){
    let add = await taskHandler.addIdFile(file.idTask,file._id);
  }
  next();
});
const FileModel = mongoose.model('File', FileSchema);
export default FileModel;