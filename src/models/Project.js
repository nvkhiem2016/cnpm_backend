import mongoose,{Schema} from 'mongoose'
import { start } from 'repl';

const ProjectSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    description:{
        type:String,
        default:''
    },
    listUser:[{
        type:Schema.Types.ObjectId,
        ref:'User'
    }],
    createBy:{
        type:Schema.Types.ObjectId,
        ref:'User',
        required:true
    },
    timeStart:{
        type:Date,
        default:Date.now
    },
    timeEnd:{
        type:Date,
        default:''
    }
})
const ProjectModel = mongoose.model('Project',ProjectSchema);
export default ProjectModel;