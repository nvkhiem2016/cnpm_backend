import mongoose, { Schema } from 'mongoose'

const ReminderSchema = new Schema({
  reminderAt: {
    type: Date
  },
  description :{
    type:String,
    default:"Reminder"
  },
  idTask:{
    type: Schema.Types.ObjectId,
    ref : "Task",
    default:null
  },
  idUser:{
    type: Schema.Types.ObjectId,
    ref : "User"
  },
  isRead:{
    type:Boolean,
    default:false
  }
})
const ReminderModel = mongoose.model('Reminder', ReminderSchema);
export default ReminderModel;