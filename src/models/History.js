import mongoose, { Schema } from 'mongoose'


const HistorySchema = new Schema({
  idTask: {
    type: Schema.Types.ObjectId,
    ref: 'Task'
  },
  createBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  createAt: {
    type: Date,
    default: Date.now
  },
  updateDisposition: {
    type: String
  },
  content: [{
    old: {
      type: String,
      default: ""
    },
    new: {
      type: String,
      default: ""
    }
  }],
})
const HistoryModel = mongoose.model('History', HistorySchema);
export default HistoryModel;