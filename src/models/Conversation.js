import mongoose, { Schema } from 'mongoose'


const ConversationSchema = new Schema({
  name: {
    type: String
  },
  listUser: [{
    type: Schema.Types.ObjectId,
    ref: "User",
    default: null
  }],
  createAt:{
    type:Date
  }
})
const ConversationModel = mongoose.model('Conversation', ConversationSchema);
export default ConversationModel;