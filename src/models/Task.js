import mongoose, { Schema } from 'mongoose'
import SendMail from '../helpers/sendmail';

const TaskSchema = new Schema({
    name: {
        type: String,
        default: ''
    },
    description: {
        type: String,
        default: ''
    },
    startOn: {
        type: Date,
    },
    deadline: {
        type: Date,
    },
    projectId: {
        type: Schema.Types.ObjectId,
        ref: "Project",
        default: null
    },
    createBy: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "User"
    },
    supervisorPerson: {
        type: Schema.Types.ObjectId,
        ref: "User",
        default: null
    },
    reponsiblePerson: [{
        type: Schema.Types.ObjectId,
        ref: "User",
        default: null
    }],
    listFile: [{
        type: Schema.Types.ObjectId,
        ref: "File",
        default: null
    }],
    // reminder:{
    //     type:Date,
    //     default:''
    // },
    status: {
        type: String, //DONE,FINISH,PROCESSING,SUPPEND
        default: 'START'
    },
    createAt: {
        type: Date,
        default: Date.now
    },
    finishAt: {
        type: Date,
        default: null
    },
    tag: String,
    isPriority: Boolean

})
TaskSchema.pre("save", async function (next) {
    var task = this;
    let data = await task.populate('projectId', ['name'])
        .populate('createBy', ['name', 'email'])
        .populate('reponsiblePerson', ['name', 'email'])
        .populate('supervisorPerson', ['name', 'email'])
        .execPopulate()
    data.subject = "New Task";
    let to = [
        ...data.reponsiblePerson.map(item => {
            return item.email
        }),
        data.supervisorPerson.email
    ]
    console.log(to)
    let mailOptions = {
        to: to, // list of receivers
        subject: data.subject, // Subject line
        html: `
          <p>Task Name : ${data.name}</p>  
          <p>Project Name : ${data.projectId.name}</p>  
          <p>Create By : ${data.createBy.name}</p>
          <p>Reponsible : ${data.reponsiblePerson.map(item => {
                return item.name
            })}</p>
          <p>Deadline : ${data.deadline}</p>
        `// plain text body
    };
    SendMail(mailOptions);
    next();
});
TaskSchema.pre("update", function (next) {
    next();
});
const TaskModel = mongoose.model('Task', TaskSchema);
export default TaskModel;