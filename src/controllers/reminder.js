import BaseController from "./base";

//import helper

//import handler
import ReminderHandler from "../handlers/reminder";

//import validateError
import ValidateError from './../errors/validateError'
//new Object

const reminderHandler = new ReminderHandler();


class ReminderController extends BaseController {
  constructor() {
    super();
  }

  /**
* Function: 
* Input: 
* OutPut: 
* Description: 
*/
  async getAllReminder(req, res, next) {
    let idUser = req.userId;
    try {
      let reminder = await reminderHandler.getAll({
        idUser
      })
      return res.onSuccess(reminder);
    } catch (error) {
      return res.onError(error);
    }
  }
  async createReminder(req, res, next) {
    let { reminderAt, idTask, description } = req.body;
    let idUser = req.userId;
    try {
      let reminder = await reminderHandler.createReminder({
        idUser, reminderAt, idTask,description
      })
      console.log(reminder)
      return res.onSuccess(reminder);
    } catch (error) {
      return res.onError(error);
    }
  }
  async editReminder(req, res, next) {
    let { _id } = req.params;
    let { reminderAt, idTask, description } = req.body;
    let idUser = req.userId;
    try {
      let reminder = await reminderHandler.editReminder({
        _id,idUser, reminderAt, idTask,description
      })
      return res.onSuccess(reminder);
    } catch (error) {
      return res.onError(error);
    }
  }
  async readReminder(req, res, next) {
    let { _id } = req.params;
    let { isRead } = req.body;
    let idUser = req.userId;
    try {
      let reminder = await reminderHandler.editReminder({
        _id,isRead
      })
      return res.onSuccess(reminder);
    } catch (error) {
      return res.onError(error);
    }
  }
}
export default ReminderController;