import BaseController from "./base";
import JWT from "jsonwebtoken";
import { secret } from "./../../config";
//import helper

//import handler
import UserHandler from "../handlers/user";

//import validateError
import ValidateError from './../errors/validateError'
//new Object

const userHandler = new UserHandler();


class UserController extends BaseController {
  constructor() {
    super();
  }

  /**
* Function: 
* Input: 
* OutPut: 
* Description: tạo token authen
*/
  async getAllUser(req, res, next) {
    try {
      let users = await userHandler.findAllUser({});
      return res.onSuccess(users);
    } catch (error) {
      return res.onError(error);
    }
  }
  async registerUser(req, res, next) {
    let { email, password } = req.body;

  }
  async getProfile(req, res, next) {
    let _id = req.userId;
    try {
      //handler login
      let user = await userHandler.findOneUser({ _id });
      return res.onSuccess(user);
    } catch (error) {
      return res.onError(error);
    }
  }
  async loginUser(req, res, next) {
    let { email, password } = req.body;
    console.log(req.body)
    try {
      //handler login
      let user = await userHandler.handlerLogin(email, password);
      if (!user) throw new ValidateError("PASSWORD_INCORRECT");
      //Initialize token
      let token = await this._signToken(user);
      return res.onSuccess(token);
    } catch (error) {
      return res.onError(error);
    }
  }
  async create(req, res, next) {
    let { email, password, name, gender, address,birthday } = req.body;
    try {
      let user = await userHandler.createUser({
        email, password, name, gender, address,birthday
      });
      return res.onSuccess(user);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
  async _signToken(user) {
    return JWT.sign(
      {
        iss: "khiemnv",
        sub: user._id,
        name: user.name,
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate() + 1)
      },
      secret.SECRET_KEY
    );
  }
}
export default UserController;