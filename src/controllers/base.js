import HttpStatus from "http-status-codes";
const autoBind = require("auto-bind");

class BaseController {
  constructor() {
    autoBind(this);
  }
  defaultResponse = {
    success: false,
    message: "",
    result: {}
  };
  response = res =>{
    return {
      onSuccess: (data,message,status)=>{
        return res.status(HttpStatus.OK || status).json({
          ...this.defaultResponse,
          success: true,
          message,
          result: data
        })
      },
      onError: error => {
        return res.status(error.status).json({
          ...this.defaultResponse,
          success: false,
          message: error.message || "UNKNOWN_ERROR"
        })
      }
    }
  }
  validateInput(req, bodySchema, querySchema, paramsSchema) {
    let errors = [];
    if (bodySchema) req.checkBody(bodySchema);
    if (querySchema) req.checkQuery(querySchema);
    if (paramsSchema) req.checkParams(paramsSchema);

    return req.getValidationResult().then(result=> {
      if(!result.isEmpty()) {
          errors = result.array().map(function (elem) {
          return elem.msg;
        });
        return errors;
      }
      return errors;
    })
  }
}
export default BaseController;