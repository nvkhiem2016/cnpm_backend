import fs from 'fs';
import BaseController from "./base";
//import helper

//import handler
import FileHandler from "../handlers/file";

//import validateError
import ValidateError from './../errors/validateError'
//new Object
const fileHandler = new FileHandler();



class FileController extends BaseController {
  constructor() {
    super();
  }
  async createFile(req, res, next) {
    try {
      //Kiểm tra upload thành công hay chưa
      let { name, idTask, idProject } = req.body;
      let createBy = req.userId;
      let fileinfor = req.files.map(file => {
        return {
          name: file.originalname,
          path: file.path
        }
      });

      let create = await fileHandler.createFile({
        name, idTask, idProject, createBy, fileinfor
      })
      return res.onSuccess(create);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
  async getAllFileByProjectId(req, res, next) {
    try {
      //Kiểm tra upload thành công hay chưa
      let { idProject } = req.params;
      let create = await fileHandler.findAllFile({
        idProject
      })
      return res.onSuccess(create);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
  async downloadFile(req, res, next) {
    try {
      //let { path } = req.params;
      let { idFile } = req.params;
      let file = await fileHandler.getFileByKeyValue("_id", idFile);
      if (!file)
        throw new ValidateError("FILE_NOT_FOUND_BY_ID");
      if (fs.existsSync(file[0].fileinfor[0].path)) {//fileinfor is array
        let file1 = await fs.createWriteStream(file[0].fileinfor[0].path);
        //res.sendFile(file[0].fileinfor[0].path, { root: './' });
        return res.download(file[0].fileinfor[0].path,file[0].fileinfor[0].name);
      } else {
        throw new ValidateError("FILE_NOT_FOUND_BY_PATH");
      }
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
}
export default FileController;