import BaseController from "./base";
//import helper

//import handler
import ProjectHandler from "../handlers/project";

//import validateError
import ValidateError from './../errors/validateError'
//new Object
const projectHandler = new ProjectHandler();



class ProjectController extends BaseController {
  constructor() {
    super();
  }

  async createProject(req, res, next) {
    let { name, listUser,description,timeAt,timeEnd,tag } = req.body;
    let createBy = req.userId;
    try {
      //listUser.push(createBy)
      let newProject = await projectHandler.createProject({
        name,listUser,createBy,description,timeAt,timeEnd,tag
      })
      return res.onSuccess(newProject);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
  async addUserProject(req, res, next) {
    let {_id} = req.params;
    let { listUser } = req.body;
    try {
      let addUserProject = await projectHandler.addUserProject({
        _id, listUser
      })
      return res.onSuccess(addUserProject);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
  async getAllProjects(req,res,next) {
    try {
      let projects = await projectHandler.findProject({})
      return res.onSuccess(projects);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
  async getAllProjectsByUser(req,res,next) {
    let userId = req.userId;
    try {
      let projects = await projectHandler.getAllProjectsByUser({
        userId
      })
      return res.onSuccess(projects);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
}
export default ProjectController;