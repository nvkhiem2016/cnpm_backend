import BaseController from "./base";
//import helper

//import handler
import ConversationHandler from "../handlers/conversation";

//import validateError
import ValidateError from './../errors/validateError'
//new Object
const conversationHandler = new ConversationHandler();



class ConversationController extends BaseController {
  constructor() {
    super();
  }

  async createConversation(req,res,next) {
    let userId = req.userId;
    try {
      let conversation = await conversationHandler.createConversation({
         
      })
      return res.onSuccess(conversation);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
  async getAllConversation(req,res,next) {
    let userId = req.userId;
    try {
      let conversation = await conversationHandler.createConversation({
         
      })
      return res.onSuccess(conversation);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
}
export default ConversationController;