import BaseController from "./base";
//import helper

//import handler
import CommentHandler from "../handlers/comment";

//import validateError
import ValidateError from './../errors/validateError'
//new Object
const commentHandler = new CommentHandler();



class CommentController extends BaseController {
  constructor() {
    super();
  }
  async createComment(req,res,next){
    let { idTask,content,createAt } = req.body;
    let createBy = req.userId;
    try {
      //listUser.push(createBy)
      let newProject = await commentHandler.createComment({
        idTask,content,createAt,createBy
      })
      return res.onSuccess(newProject);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
  async getAllComment(req,res,next){
    let { idTask } = req.params;
    
    try {
      //listUser.push(createBy)
      let newProject = await commentHandler.getAllComment({
        idTask
      })
      return res.onSuccess(newProject);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
}
export default CommentController;