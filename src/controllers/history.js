import BaseController from "./base";
//import helper

//import handler
import HistoryHandler from "../handlers/history";

//import validateError
import ValidateError from './../errors/validateError'
//new Object
const historyHandler = new HistoryHandler();



class HistoryController extends BaseController {
  constructor() {
    super();
  }
  async createHistory(req, res, next) {
    let { newData, oldData, createAt,updateDisposition,taskId } = req.body;
    let userId = req.userId;
    try {
      //let updateDisposition = "STATUS";
      let create = await historyHandler.createHistory({
        userId, taskId, newData, oldData, updateDisposition, createAt
      })
      return res.onSuccess(create);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
  async getAllHistory(req, res, next) {
    let { idTask } = req.params;
    try {
      //listUser.push(createBy)
      let histories = await historyHandler.getAllHistory({
        idTask
      })
      return res.onSuccess(histories);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
}
export default HistoryController;