import moment from 'moment';
import BaseController from "./base";
//import helper
import SendMail from './../helpers/sendmail'
//import handler
import TaskHandler from "../handlers/task";
import HistoryHandler from "../handlers/history";
import UserHandler from "../handlers/user";

//import validateError
import ValidateError from './../errors/validateError'
//new Object
const taskHandler = new TaskHandler();
const historyHandler = new HistoryHandler();
const userHandler = new UserHandler();




class TaskController extends BaseController {
  constructor() {
    super();
  }

  async createTask(req, res, next) {
    let { name, startOn, deadline, description, supervisorPerson, reponsiblePerson, reminder, projectId, createAt, tag, createBy } = req.body;
    //let createBy = req.userId;
    try {
      // deadline = moment(deadline).format('YYYY-MM-DD HH:mm');//format lại dâte
      let newTask = await taskHandler.create({
        name, startOn, deadline, description, supervisorPerson, reponsiblePerson, reminder, projectId, createBy, createAt, tag
      })
      return res.onSuccess(newTask);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
  // Sửa thông tin task
  async editTask(req, res, next) {
    let { taskId } = req.params;
    let { name, startOn, deadline, description, supervisorPerson, reponsiblePerson, reminder, projectId, createBy, tag } = req.body;
    try {
      let editTask = await taskHandler.update({
        taskId, name, description, startOn, deadline, supervisorPerson, reponsiblePerson, reminder, projectId, createBy, tag
      })
      return res.onSuccess(editTask);
    } catch (error) {
      return res.onError(error);
    }
  }
  //Chỉnh sửa trạng thái của task
  async editStatusTask(req, res, next) {
    let { status, oldStatus, createAt } = req.body;
    let { taskId } = req.params;
    let userId = req.userId;
    try {
      let editTask = await taskHandler.update({
        status, taskId,
      })
      //Send mail for supervisorPerson
      if (status === "DONE") {
        editTask.subject = "Done Task";
        let to = [
          editTask.supervisorPerson.email
        ]
        let mailOptions = {
          to: to, // list of receivers
          subject: editTask.subject, // Subject line
          html: `
          <p>Task Name : ${editTask.name}</p>  
          <p>Project Name : ${editTask.projectId.name}</p>  
          <p>Create By : ${editTask.createBy.name}</p> 
          <p>Reponsible : ${editTask.reponsiblePerson.map(item => {
            return item.name
          })}</p>
          <p>SupervisorPerson : ${editTask.supervisorPerson.name}</p>
          <p>Deadline : ${editTask.deadline}</p>
        `// plain text body
        };
        SendMail(mailOptions);
      }
      return res.onSuccess(editTask);
    } catch (error) {
      console.log(error)
      return res.onError(error);
    }
  }
  //Lấy ds task theo reponsible của user - Alltask bên react
  async getAllTaskByReponsile(req, res, next) {
    let userId = req.userId;
    try {
      let listTask = await taskHandler.getAllTask({
        userId
      })
      return res.onSuccess(listTask);
    } catch (error) {
      return res.onError(error);
    }
  }
  //Lấy ds task thuộc project
  async getAllTaskByProjectId(req, res, next) {
    let { projectId } = req.params;

    try {
      let listTask = await taskHandler.getAllTaskByProjectId({
        projectId
      })

      return res.onSuccess(listTask);
    } catch (error) {
      return res.onError(error);
    }
  }
  async finishTask(req, res, next) {
    let { taskId } = req.params;
    let { finishAt } = req.body;
    try {
      let editTask = await taskHandler.update({ taskId,finishAt })
      return res.onSuccess(editTask);
    } catch (error) {
      return res.onError(error);
    }
  }
}
export default TaskController;