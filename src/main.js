import initExpress from './boot/initExpress';
import initMongo from './boot/initMongo';

const runApp = async ()=>{
    try {
        await initExpress();
        await initMongo();
    } catch (error) {
        console.log(error);
    }
}
runApp();