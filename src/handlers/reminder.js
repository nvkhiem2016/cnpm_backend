import Base from "./base";

//import models
import Reminder from "../models/Reminder";
//import validateError
import ValidateError from './../errors/validateError'
class ReminderHandler extends Base {
  constructor() {
    super();
  }
  async getAll(data) {
    let reminder = await Reminder.find(
      { ...data })
      .populate('idTask', ['name'])
      .populate('idUser', ['name']);
    return reminder;
  }
  async createReminder(data) {
    let create = await Reminder.create(
      { ...data })
    create = create.populate({
      path: "idTask",
      select: 'name'
    }).populate({
      path: "idUser",
      select: 'name'
    }).execPopulate()
    return create;
  }
  async editReminder(data) {
    let edit = await Reminder.findOneAndUpdate(
      { _id: data._id },
      { $set: { ...data } },
      {
        new: true
      })
    edit = edit.populate({
      path: "idTask",
      select: 'name'
    }).populate({
      path: "idUser",
      select: 'name'
    }).execPopulate()
    if (edit)
      return edit;
    return false;
  }
  async readReminder(data) {
    let read = await Reminder.findOneAndUpdate({
      _id: data._id
    }, {
        ...data
      })
    return read;
  }
}
export default ReminderHandler;
