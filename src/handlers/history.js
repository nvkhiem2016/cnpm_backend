import Base from "./base";

//import models
import History from "../models/History";

class HistoryHandler extends Base {
  constructor() {
    super();
  }
  async createHistory(data) {
    let create = await History.create({ 
      idTask:data.taskId,
      createBy:data.userId,
      createAt:data.createAt,
      updateDisposition:data.updateDisposition,
      content:{
        old:data.oldData,
        new:data.newData
      }
    })
    create = create
      .populate('projectId', ['name'])
      .populate('createBy', ['name'])
      .populate('reponsiblePerson', ['name'])
      .execPopulate()
    if (create)
      return create;
    return false;
  }
  async getAllHistory(data) {
    let historys = await History.find({
      idTask: data.idTask
    }).populate('idTask', ['name'])
      .populate('createBy', ['name'])
    if (historys)
      return historys;
    return false;
  }

}
export default HistoryHandler;
