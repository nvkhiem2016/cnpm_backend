import Base from "./base";

//import models
import User from "../models/User";
//import validateError
import ValidateError from './../errors/validateError'
class UserHandler extends Base {
  constructor() {
    super();
  }
  async findOneUser(data) {
    let user = await User.findOne({
      ...data
    })
    return user;
  }
  async findByKeyValue(key,value) {
    let user = await User.findOne({
      [key]:value
    })
    return user;
  }
  async findAllUser(data){
    let users = await User.find({},['name','_id','email'])
    return users;
  }
  async handlerLogin(email, password) {
    let user = await User.findOne({email})
    console.log(user)
    if (!user) throw new ValidateError("USER_NAME_NOT_FOUND");
    const isPassValid = user.password==password?user:false;
    if (!isPassValid) return false;
    return user;
  }
  async createUser(data) {
		let created = await User.create({
			...data
		});
		return created;
	}
}
export default UserHandler;
