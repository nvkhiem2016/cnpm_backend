import Base from "./base";

//import models
import Task from "../models/Task";
import Project from "../models/Project"
//import validateError
import ValidateError from './../errors/validateError'
class TaskHandler extends Base {
  constructor() {
    super();
  }
  async create(data) {
    let created = await Task.create({
      ...data
    })
    created = created
      .populate('projectId', ['name'])
      .populate('createBy', ['name'])
      .populate('reponsiblePerson', ['name'])
      .populate('supervisorPerson', ['name'])
      .populate('listFile', ['fileinfor'])
      .execPopulate()
    return created;
  }
  async update(data) {
    // let isCreate = await this.isCreateByTask({createBy:data.createBy})
    // if(!isCreate) throw new ValidateError("YOU_NOT_OWER");
    let taskId = data.taskId;
    let updated = await Task.findOneAndUpdate(
      { _id: taskId },
      {
        $set: { ...data }
      },
      {
        new: true
      }
    );
    updated = updated
      .populate('projectId', ['name'])
      .populate('createBy', ['name'])
      .populate('reponsiblePerson', ['name', 'email'])
      .populate('supervisorPerson', ['name', 'email'])
      .populate('listFile', ['fileinfor'])
      .execPopulate()
    return updated;
  }
  async isCreateByTask(data) { //{id,createBy}
    let isCreate = await Task.findOne({ ...data });
    if (!isCreate) return false;
    return true;
  }
  async getAllTask(data) {
    let list = await Task.find({
      reponsiblePerson: data.userId
    })
      .populate('projectId', ['name'])
      .populate('createBy', ['name', 'email'])
      .populate('reponsiblePerson', ['name', 'email'])
      .populate('supervisorPerson', ['name'])
      .populate('listFile', ['fileinfor'])

    return list;
  }
  //Lấy list task theo projectId
  async getAllTaskByProjectId(data) {
    let list = await Task.find({
      projectId: data.projectId
    })
      .populate('projectId', ['name'])
      .populate('createBy', ['name', 'email'])
      .populate('reponsiblePerson', ['name', 'email'])
      .populate('supervisorPerson', ['name'])
      .populate('listFile', ['fileinfor'])
    return list;
  }
  async addIdFile(id, data) {
    let task = await Task.findOneAndUpdate({
      _id: id
    }, {
        $push: {
          listFile: data
        }
      })
    return task;
  }
  async setFinish(id, data) {
    let task = await Task.findOneAndUpdate({
      _id: id
    }, {
      finishAt:data.finishAt
      })
    return task;
  }
}
export default TaskHandler;
