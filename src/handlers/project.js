import Base from "./base";

//import models
import Project from "../models/Project";

class ProjectHandler extends Base {
  constructor() {
    super();
  }
  async findProject(data) { // data:dữ liệu để kt VD : _id,name
    let projects = await Project.find({
      ...data
    })
      .populate({
        path: "listUser",
        select: 'name'
      })
      .populate({
        path: "createBy",
        select: 'name'
      })
    if (projects.length > 0)
      return projects;
    return false;
  }
  async getAllProjectsByUser(data) {

    let projects = await Project.find({
      listUser: data.userId
    }, ['name', '_id','listUser']).populate('listUser',['name','email','_id'])
    return projects;
  }
  async createProject(data) {
    let create = await Project.create({ ...data })

    create = create.populate({
      path: "listUser",
      select: 'name'
    })
      .populate({
        path: "createBy",
        select: 'name'
      }).execPopulate()
    if (create)
      return create;
    return false;
  }
  async addUserProject(data) {
    let edit = await Project.findOneAndUpdate(
      { _id: data._id },
      { $set: { listUser: data.listUser } },
      {
        new: true
      })
    edit = edit.populate({
      path: "listUser",
      select: 'name'
    }).populate({
        path: "createBy",
        select: 'name'
      }).execPopulate()
    if (edit)
      return edit;
    return false;
  }
  async updateProject(id, data) {
    let update = await Project.findOneAndUpdate(
      { ...id }, 
      { $set:{...data }},
      {
        new:true
      });
    if (update)
      return update;
    return false;
  }
  async deleteProject(data) {
    let deleted = await Project.findOneAndRemove({ ...data });
    if (deleted)
      return deleted;
    return false;
  }
}
export default ProjectHandler;
