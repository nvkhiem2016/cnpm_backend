import Base from "./base";

//import models
import Conversation from "../models/Conversation";

class ConversationHandler extends Base {
  constructor() {
    super();
  }
  async createConversation(data) {
    let create = await Conversation.create({ ...data })

    create = create.populate({
      path: "listUser",
      select: 'name'
    }).execPopulate()
    if (create)
      return create;
    return false;
  }
  async getAllConversation(data) {
    let conversations = await Conversation.find({
      idTask: data.idTask
    }).populate('idTask', ['name'])
      .populate('createBy', ['name'])
    if (conversations)
      return conversations;
    return false;
  }

}
export default ConversationHandler;
