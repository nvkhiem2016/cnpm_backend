const autoBind = require("auto-bind");
class Base {
  constructor() {
    autoBind(this);
  }
}
module.exports = Base;
