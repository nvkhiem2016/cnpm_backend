import Base from "./base";

//import models
import Comment from "../models/Comment";

class CommentHandler extends Base {
  constructor() {
    super();
  }
  async createComment(data) {
    let create = await Comment.create({ ...data })

    create = create.populate({
      path: "createBy",
      select: 'name'
    }).execPopulate()
    if (create)
      return create;
    return false;
  }
  async getAllComment(data) {
    let comments = await Comment.find({
      idTask: data.idTask
    }).populate('idTask', ['name'])
      .populate('createBy', ['name'])
    if (comments)
      return comments;
    return false;
  }

}
export default CommentHandler;
