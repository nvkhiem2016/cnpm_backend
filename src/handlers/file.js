import Base from "./base";

//import models
import File from "../models/File";

class FileHandler extends Base {
  constructor() {
    super();
  }
  async createFile(data) {
    console.log(data)
    let create = await File.create({
      ...data
    })
    create = create
      .populate('idProject', ['name'])
      .populate('idTask', ['name'])
      .populate('createBy', ['name'])
      .populate('reponsiblePerson', ['name'])
      .execPopulate()
    if (create)
      return create;
    return false;
  }
  async getFileByKeyValue(key,value) {
    let getFile = await File.find({
      [key]:value
    })
    if (getFile)
      return getFile;
    return false;
  }
  async findAllFile(data) {
    let files = await File.find({
      ...data
    }).populate({
      path: "idTask",
      select: 'name'
    }).populate({
      path: "idProject",
      select: 'name'
    }).populate({
      path: "createBy",
      select: 'name'
    })

    if (files)
      return files;
    return false;
  }

}
export default FileHandler;
