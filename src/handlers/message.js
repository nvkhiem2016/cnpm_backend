import Base from "./base";

//import models
import Message from "../models/Message";

class MessageHandler extends Base {
  constructor() {
    super();
  }
  async createMessage(data) {
    let create = await Message.create({ ...data })

    create = create.populate({
      path: "createBy",
      select: 'name'
    }).execPopulate()
    if (create)
      return create;
    return false;
  }
  async getAllMessage(data) {
    let messages = await Message.find({
      idTask: data.idTask
    }).populate('idTask', ['name'])
      .populate('createBy', ['name'])
    if (messages)
      return messages;
    return false;
  }

}
export default MessageHandler;
