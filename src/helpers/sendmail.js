import nodemailer from 'nodemailer';
import moment from 'moment'
import mailer from './../../config/mailer';
const transporter = nodemailer.createTransport({
  ...mailer
});

const SendMail = (mailOptions) => {
  transporter.sendMail(mailOptions, function (err, info) {
    if (err)
      console.log(err)
    else
      console.log(info);
  });
}
export default SendMail;