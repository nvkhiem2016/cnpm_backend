const autoBind = require("auto-bind");
const expressValidator = require('express-validator')
class ValidationManager {
  constructor() {
    autoBind(this);
  }

  provideDefaultValidator() {
    return expressValidator({
      errorFormatter: ValidationManager.errorFormatter
    })
  }

  static errorFormatter(param, msg, value) {
    let namespace = param.split('.'),
      root = namespace.shift(),
      formParam = root

    while (namespace.length) {
      formParam += '[' + namespace.shift() + ']'
    }
    return {
      param: formParam,
      msg: msg,
      value: value
    }
  }
}
module.exports = ValidationManager

