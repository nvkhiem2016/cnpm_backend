const BaseError = require('./base');

class ValidateError extends BaseError {
    constructor(message) {
        super(message, 404);
    }
}

module.exports = ValidateError;