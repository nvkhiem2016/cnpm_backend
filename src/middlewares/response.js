
import BaseController from "../controllers/base";
const base = new BaseController();

const response = (req, res, next) => {
    res.onSuccess = base.response(res).onSuccess;
    res.onError =  base.response(res).onError;
    return next();
  };

export default response;