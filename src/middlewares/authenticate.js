var passport = require("passport");
const authenticate = () => {
  return async function(req, res, next) {
    passport.authenticate("jwt", (error, user) => {
      if (!error && user) {
        req.userId = user._id;
        next();
      } else {
        return res.status(404).json({
          success: false,
          message:
            error && error.message ? error.message : "AUTHENTICATION_FAILED",
          result: {}
        });
      }
    })(req, res, next);
  };
};
export default authenticate;
