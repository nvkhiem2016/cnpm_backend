import multer from 'multer';
import moment from 'moment'
import fs from 'fs';
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `./public/uploads`)
  },
  filename: (req, file, cb) =>
   {
    console.log(file, "filename")
    let fullname = file.originalname.split('.');
    let extension = fullname[fullname.length -1];
    fullname.splice(fullname.length - 1,1);
    fullname = fullname.join();
    let path =`${fullname}-${Date.now()}.${extension}`
    cb(null, path)
    //   cb(null, file.fieldname + '-' + Date.now())
    
  }
});
const upload = multer({ storage: storage });
export default upload;