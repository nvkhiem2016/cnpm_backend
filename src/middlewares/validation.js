import ValidateError from "../errors/validateError";

function validateInput(req, bodySchema, querySchema, paramsSchema) {
  let errors = [];
  if (bodySchema) req.checkBody(bodySchema);
  if (querySchema) req.checkQuery(querySchema);
  if (paramsSchema) req.checkParams(paramsSchema);

  return req.getValidationResult().then(result => {
    if (!result.isEmpty()) {
      errors = result.array().map(function(elem) {
        return elem.msg;
      });
      return errors;
    }
    return errors;
  });
}

const validation = (bodySchema, querySchema, paramsSchema) => {
  return async (req, res, next) => {
    try {
      let errors = await validateInput(
        req,
        bodySchema,
        querySchema,
        paramsSchema  
      );
      if (errors.length > 0) throw new ValidateError(errors);
      next();
    } catch (error) {
      return res.status(error.status || 404).json({
        success: false,
        message:
          error && error.message ? error.message : "VALIDATE_INPUT_FAILED",
        result: {}
      });
    }
  };
};

export default validation;
