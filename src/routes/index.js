var express = require("express");
var router = express.Router();
import Routerv1 from './v1/index';

router.use('/api/v1',Routerv1);

export default router;