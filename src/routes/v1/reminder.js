const router = require("express").Router();


//import validate Shcemas

//import controller
import ReminderController from "../../controllers/reminder";
//import middlewares
import authenticate from "../../middlewares/authenticate";
// import validation from "../../middlewares/validation";
let reminderController = new ReminderController();
//GET METHOH
router.get('/', 
  authenticate(), 
  reminderController.getAllReminder);
//POST METHOD
router.post('/', 
  authenticate(), 
  reminderController.createReminder);
//PUT METHOD
router.put('/:_id', 
  authenticate(), 
  reminderController.editReminder);
router.put('/readReminder/:_id', 
  authenticate(), 
  reminderController.readReminder);
//PUT METHOD

//DELETE METHOD

export default router;
