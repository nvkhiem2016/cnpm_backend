const router = require("express").Router();


//import validate Shcemas
import {
  VALIDATE_PROJECTID_SCHEMA, VALIDATE_TASKID_SCHEMA
} from "../../validateSchemas/common";
//import controller
import TaskController from "../../controllers/task";
//middlewares
import authenticate from "../../middlewares/authenticate";
import validation from "../../middlewares/validation";
let taskController = new TaskController();
//GET METHOH
router.get('/getAllTask', authenticate(), taskController.getAllTaskByReponsile);
router.get('/getAllTaskByProjectId/:projectId', authenticate(), taskController.getAllTaskByProjectId);
//POST METHOD
router.post('/',
  authenticate(),
  validation({

  }),
  taskController.createTask);

//PUT METHOD
router.put('/:taskId',
  authenticate(),
  validation({}, {}, {
    ...VALIDATE_TASKID_SCHEMA
  }),
  taskController.editTask);
router.put('/setStatusTask/:taskId',
  authenticate(),
  taskController.editStatusTask);
router.put('/finishTask/:taskId',
  authenticate(),
  taskController.finishTask);
export default router;
