const router = require("express").Router();


//import validate Shcemas

//import controller
import ConversationController from "../../controllers/conversation";
//import middlewares
import authenticate from "../../middlewares/authenticate";
// import validation from "../../middlewares/validation";
let conversationController = new ConversationController();
//GET METHOH
router.get('/', authenticate(), conversationController.getAllConversation);

//POST METHOD

router.post('/', authenticate(), conversationController.createConversation);
//PUT METHOD

export default router;
