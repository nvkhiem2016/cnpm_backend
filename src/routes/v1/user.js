const router = require("express").Router();


//import validate Shcemas

//import controller
import UserController from "../../controllers/user";
//import middlewares
import authenticate from "../../middlewares/authenticate";
// import validation from "../../middlewares/validation";
let userController = new UserController();
//GET METHOH
router.get('/',userController.getAllUser);
router.get('/getProfile',authenticate(),userController.getProfile);
//POST METHOD
router.post('/login',userController.loginUser);
router.post('/',userController.create);
//PUT METHOD

export default router;
