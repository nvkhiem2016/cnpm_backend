const router = require("express").Router();


//import validate Shcemas

//import controller
import CommentController from "../../controllers/comment";
//import middlewares
import authenticate from "../../middlewares/authenticate";
// import validation from "../../middlewares/validation";
let commentController = new CommentController();
//GET METHOH
router.get('/:idTask',authenticate(), commentController.getAllComment);
//POST METHOD
router.post('/',authenticate(), commentController.createComment);

//PUT METHOD


//DELETE METHOD

export default router;
