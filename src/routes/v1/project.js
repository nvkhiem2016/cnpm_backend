const router = require("express").Router();


//import validate Shcemas

//import controller
import ProjectController from "../../controllers/project";
//import middlewares
import authenticate from "../../middlewares/authenticate";
// import validation from "../../middlewares/validation";
let projectController = new ProjectController();
//GET METHOH
router.get('/getAllProjects', projectController.getAllProjects);
router.get('/getAllProjectsByUser', 
  authenticate(), 
  projectController.getAllProjectsByUser);
//POST METHOD
router.post('/', 
  authenticate(), 
  projectController.createProject);
//PUT METHOD
router.put('/addUser/:_id', 
  authenticate(), 
  projectController.addUserProject);

//DELETE METHOD

export default router;
