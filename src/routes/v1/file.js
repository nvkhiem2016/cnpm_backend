const router = require("express").Router();


//import validate Shcemas

//import controller
import FileController from "../../controllers/file";
//import middlewares
import authenticate from "../../middlewares/authenticate";
import upload from './../../middlewares/upload';
// import validation from "../../middlewares/validation";
let fileController = new FileController();
//GET METHOH
router.get('/:idProject',authenticate(), fileController.getAllFileByProjectId);
router.get('/download/downloadFile/:idFile',authenticate(), fileController.downloadFile);
//POST METHOD
router.post('/',authenticate(),upload.array('files'), fileController.createFile);

//PUT METHOD


//DELETE METHOD

export default router;
