var express = require("express");
var Routerv1 = express.Router();
//import router
import project from './project';
import task from './task';
import user from './user';
import reminder from './reminder'
import comment from './comment'
import history from './history'
import conversation from './conversation'
import file from './file'


Routerv1.get('/',(req,res)=>{res.status(200).json({msg:"OKE - TEST"})})
Routerv1.use('/project',project)
Routerv1.use('/task',task)
Routerv1.use('/user',user)
Routerv1.use('/reminder',reminder)
Routerv1.use('/comment',comment)
Routerv1.use('/conversation',conversation)
Routerv1.use('/history',history)
Routerv1.use('/file',file)



export default Routerv1;