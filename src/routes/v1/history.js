const router = require("express").Router();


//import validate Shcemas

//import controller
import HistoryController from "../../controllers/history";
//import middlewares
import authenticate from "../../middlewares/authenticate";
// import validation from "../../middlewares/validation";
let historyController = new HistoryController();
//GET METHOH
router.get('/:idTask',authenticate(), historyController.getAllHistory);
//POST METHOD
router.post('/',authenticate(), historyController.createHistory);

//PUT METHOD


//DELETE METHOD

export default router;
