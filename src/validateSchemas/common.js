export const VALIDATE_PROJECTID_SCHEMA = {
    projectId : {
        isMongoId: {
            errorMessage : "_ID_NOT_VALID"
        }
    }

}
export const VALIDATE_TASKID_SCHEMA = {
    taskId : {
        isMongoId: {
            errorMessage : "_ID_NOT_VALID"
        }
    }

}
export const VALIDATE_USERID_SCHEMA = {
    userId : {
        isMongoId: {
            errorMessage : "_ID_NOT_VALID"
        },
        exists: {
            errorMessage : "ID_NOT_EMPTY"
        }
    }
}


