export const VALIDATE_FULLNAME_SCHEMA = {
    fullname : {
        exists: {
            errorMessage : "FULL_NAME_NOT_EMPTY"
        }
    }
}
export const VALIDATE_PHONE_SCHEMA = {
    phoneNumber : {
        exists: {
            errorMessage : "PHONE_NUMBER_NOT_EMPTY"
        }
    }
}
export const VALIDATE_BIRTHDATE_SCHEMA = {
    birthDate : {
        exists: {
            errorMessage : "BIRTHDATE_NOT_EMPTY"
        }
    }
}
export const VALIDATE_IDENTIFICATION_NUMBER_SCHEMA = {
    identificationNumber : {
        exists: {
            errorMessage : "IDENTIFICATION_NUMBER_NOT_EMPTY"
        }
    }
}
